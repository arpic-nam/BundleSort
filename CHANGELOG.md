# BundleSort Plugin Changelog

## [1.4.0] - 2024-08-04

### Fixed
- Follow codestyle option "Insert space around key-value delimiter"
- Warning related to ActionUpdateThread

### Changed
- Migrated project to Intellij Platform Gradle Plugin 2.0.0
- Minimal supported version to 2022.3
- Follow semantic versioning
- Gradle version upgraded to 8.9.0
- Java version upgraded to JDK 17

## [1.3.0] - 2022-02-03

### Fixed
- An issue when bundle file contains blank / empty entries\
  Thanks to @robertoschwald for the contribution

## [1.2.0] - 2020-06-10

### Added
- Integration with Git4Idea plugin: A new option to sort resources is now present in the Git "Commit Changes" window

### Fixed
- An issue where multi-line support would fail on lines with property-like format


## [1.1.0] - 2020-05-09

### Added
- Support for multi-line resources
```
multiline-resource = This\
is\
multiline
```

- Support for most cases defined in https://docs.oracle.com/javase/10/docs/api/java/util/Properties.html#load(java.io.Reader)
  
    Specifically:
     - ! as comment marker
     - : or whitespace as key value separator
     - Escaped separators in key or value

- Line number and content for clarity in error message

### Changed
- Minimal supported version to 2019.1 (Android Studio 3.5)

- Testing framework to Kotest
