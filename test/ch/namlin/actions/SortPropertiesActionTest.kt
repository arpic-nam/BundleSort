package ch.namlin.actions

import com.intellij.application.options.CodeStyle
import com.intellij.lang.properties.psi.codeStyle.PropertiesCodeStyleSettings
import com.intellij.openapi.ui.TestDialog
import com.intellij.openapi.ui.TestDialogManager
import com.intellij.psi.PsiFile
import com.intellij.testFramework.fixtures.BasePlatformTestCase
import org.junit.jupiter.api.Assertions

class SortPropertiesActionTest : BasePlatformTestCase() {

    override fun setUp() {
        super.setUp()
        TestDialogManager.setTestDialog(TestDialog.OK)
    }

    fun testGetBundle() {
        val file = myFixture.copyDirectoryToProject("Input", ".")
        val action = SortPropertiesAction()
        val bundle = action.getBundle(file.findChild("Resources.properties") ?: throw IllegalStateException())
        Assertions.assertEquals(2, bundle.size)
    }

    fun testSort() {
        val testSettings = CodeStyle.createTestSettings()
        val propertyStyleSettings = testSettings.getCustomSettings(PropertiesCodeStyleSettings::class.java)
        propertyStyleSettings.SPACES_AROUND_KEY_VALUE_DELIMITER = true

        CodeStyle.setTemporarySettings(project, testSettings)

        myFixture.configureByFiles("Input/Resources.properties", "Input/Resources_fr.properties")
        myFixture.performEditorAction("BundleSort.SortPropertiesAction")

        myFixture.checkResultByFile("Results/Resources.properties")
        myFixture.checkResultByFile("Input/Resources_fr.properties", "Results/Resources_fr.properties", false)

        CodeStyle.dropTemporarySettings(project)
    }

    fun testSort_no_spaces() {
        val testSettings = CodeStyle.createTestSettings()
        val propertyStyleSettings = testSettings.getCustomSettings(PropertiesCodeStyleSettings::class.java)
        propertyStyleSettings.SPACES_AROUND_KEY_VALUE_DELIMITER = false

        CodeStyle.setTemporarySettings(project, testSettings)

        myFixture.configureByFiles("Input/Resources.properties", "Input/Resources_fr.properties")
        myFixture.performEditorAction("BundleSort.SortPropertiesAction")

        myFixture.checkResultByFile("Results/Resourcesnospaces.properties")
        myFixture.checkResultByFile("Input/Resources_fr.properties", "Results/Resourcesnospaces_fr.properties", false)

        CodeStyle.dropTemporarySettings(project)
    }

    fun testIdempotency() {
        val file = myFixture.configureByFile("Input/Idempotent.properties")
        sortFile(file, true)
        myFixture.checkResultByFile("Results/Idempotent.properties")

        val fileNoSpaces = myFixture.configureByFile("Input/Idempotentnospaces.properties")
        sortFile(fileNoSpaces, false)
        myFixture.checkResultByFile("Results/Idempotentnospaces.properties")
    }

    private fun sortFile(file: PsiFile, spacesAroundDelimiter: Boolean) {
        SortPropertiesAction().sortFile(
            file.virtualFile ?: throw java.lang.IllegalStateException(),
            spacesAroundDelimiter
        )
    }

    override fun getTestDataPath(): String = "testData"
}
