import org.jetbrains.changelog.Changelog
import org.jetbrains.intellij.platform.gradle.TestFrameworkType

plugins {
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.ij.platform)
    alias(libs.plugins.ij.changelog)
}

version = "1.4.0"

sourceSets {
    main {
        kotlin {
            setSrcDirs(listOf("src"))
        }
        resources {
            setSrcDirs(listOf("resources"))
        }
    }
    test {
        kotlin {
            setSrcDirs(listOf("test"))
        }
    }
}

kotlin {
    jvmToolchain(17)
}

intellijPlatform {
    buildSearchableOptions = false
    pluginConfiguration {
        id = "ch.namlin.plugin.bundlesort"
        name = "Bundle Sort"
        description = "Sort Java resources bundles alphabetically."

        vendor {
            name = "Namlin"
        }

        changeNotes = provider { changelog.renderItem(changelog.getLatest(), Changelog.OutputType.HTML) }
        ideaVersion {
            sinceBuild = "223"
            untilBuild = provider { null }
        }
    }

    signing {
        certificateChain = providers.environmentVariable("JB_CERT_CHAIN")
        privateKey = providers.environmentVariable("JB_PRIVATE_KEY")
        password = providers.environmentVariable("JB_PK_PW")
    }

    publishing {
        token = providers.environmentVariable("JB_API_TOKEN")
    }
}

tasks.register("gitlabReleaseNotes", GitlabChangelog::class) {
    changelogText.set(provider { changelog.renderItem(changelog.getLatest(), Changelog.OutputType.MARKDOWN)})
    changelogFile.set(layout.buildDirectory.file("gitlab-changelog.md"))
}

tasks.named<Test>("test") {
    useJUnitPlatform()
}

repositories {
    mavenCentral()
    intellijPlatform {
        defaultRepositories()
    }
}

dependencies {
    testImplementation(libs.kotest.runner)
    testImplementation(libs.kotest.assertions)
    testRuntimeOnly(libs.junit.vintage)
    testImplementation(libs.junit4)

    intellijPlatform {
        intellijIdeaCommunity("2022.3")
        instrumentationTools()
        pluginVerifier()
        zipSigner()
        bundledPlugins(listOf("com.intellij.java", "Git4Idea", "com.intellij.properties"))
        testFramework(TestFrameworkType.Platform)
    }
}

abstract class GitlabChangelog : DefaultTask() {

    @get:Input
    abstract val changelogText: Property<String>
    @get:OutputFile
    abstract val changelogFile: RegularFileProperty

    @TaskAction
    fun writeChangelog() {
        changelogFile.asFile.get().writeText(changelogText.get())
    }
}