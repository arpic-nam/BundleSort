package ch.namlin.util

import com.intellij.application.options.CodeStyle
import com.intellij.lang.properties.psi.codeStyle.PropertiesCodeStyleSettings
import com.intellij.psi.PsiFile

object SettingsHelper {

    fun spacesAroundDelimiter(file: PsiFile) : Boolean {
        val languageSettings = CodeStyle.getCustomSettings(file, PropertiesCodeStyleSettings::class.java)
        val spacesAroundDelimiter = languageSettings.SPACES_AROUND_KEY_VALUE_DELIMITER
        return spacesAroundDelimiter
    }

}