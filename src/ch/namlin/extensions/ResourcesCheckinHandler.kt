package ch.namlin.extensions

import ch.namlin.actions.BundleSortException
import ch.namlin.util.PropertyBundleSorter
import ch.namlin.util.SettingsHelper
import com.intellij.CommonBundle
import com.intellij.ide.util.PropertiesComponent
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.Messages
import com.intellij.openapi.vcs.CheckinProjectPanel
import com.intellij.openapi.vcs.changes.CommitContext
import com.intellij.openapi.vcs.changes.CommitExecutor
import com.intellij.openapi.vcs.changes.ui.BooleanCommitOption
import com.intellij.openapi.vcs.checkin.CheckinHandler
import com.intellij.openapi.vcs.checkin.VcsCheckinHandlerFactory
import com.intellij.openapi.vcs.ui.RefreshableOnComponent
import com.intellij.openapi.vfs.VfsUtil
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.psi.PsiManager
import com.intellij.util.PairConsumer
import com.intellij.util.ui.UIUtil
import git4idea.GitVcs
import org.jetbrains.annotations.Nullable
import java.io.File
import java.util.function.Consumer

class ResourcesCheckinHandlerFactory : VcsCheckinHandlerFactory(GitVcs.getKey()) {
	override fun createVcsHandler(panel: CheckinProjectPanel, commitContext: CommitContext): CheckinHandler {
		return ResourcesCheckinHandler(panel)
	}

	class ResourcesCheckinHandler(private val panel: CheckinProjectPanel) : CheckinHandler() {
		private val myPanel: CheckinProjectPanel = panel
		private var sortResources
			get() = PropertiesComponent.getInstance(panel.project).getBoolean("ch.namlin.jiragit.sortResources", false)
			set(value) = PropertiesComponent.getInstance(panel.project).setValue("ch.namlin.jiragit.sortResources", value)
		private val propertyBundleSorter = PropertyBundleSorter()

		@Nullable
		override fun getBeforeCheckinConfigurationPanel(): RefreshableOnComponent {
			return BooleanCommitOption(myPanel, "Sort resources", true, { sortResources},
					Consumer { value: Boolean -> sortResources = value })
		}

		override fun beforeCheckin(executor: CommitExecutor?, additionalDataConsumer: PairConsumer<Any, Any>?): ReturnResult {

			if (!sortResources) return ReturnResult.COMMIT

			val failedFiles = tryAndSortFiles(panel.project)
			if (failedFiles.isNotEmpty()) {
				val commitButtonText = if (executor != null) executor.actionText else panel.commitActionName
				val answer = Messages.showYesNoDialog(panel.project, "Some resources failed to be sorted. Would you like to commit anyway?",
						"Sort Resources", commitButtonText, CommonBundle.getCancelButtonText(), UIUtil.getWarningIcon())
				return if (answer == 1) ReturnResult.CANCEL else ReturnResult.COMMIT
			}
			return ReturnResult.COMMIT
		}

		private fun tryAndSortFiles(project: Project): MutableList<File> {
			val failedFiles = mutableListOf<File>()
			var spacesAroundDelimiter: Boolean? = null
			for (file in myPanel.files) {
				try {
					val virtualFile = VfsUtil.findFileByIoFile(file, true)
					if (file.exists() && virtualFile != null && virtualFile.fileType.name == "Properties") {
						if (spacesAroundDelimiter == null) {
							PsiManager.getInstance(project).findFile(virtualFile)?.let {
								spacesAroundDelimiter = SettingsHelper.spacesAroundDelimiter(it)
							}
						}
						sortFile(file, virtualFile, spacesAroundDelimiter)
					}
				} catch (bundleSortException: BundleSortException) {
					failedFiles.add(file)
				}
			}
			return failedFiles
		}


		private fun sortFile(resourceFile: File, virtualFile: VirtualFile, spacesAroundDelimiter: Boolean?) {
			val fileText = String(resourceFile.readBytes())
			if (fileText.isNotEmpty()) {
				val separator = virtualFile.detectedLineSeparator ?: return
				val newDocumentContent = propertyBundleSorter.sortAndConvertString(
                    fileText,
                    separator,
                    spacesAroundDelimiter ?: true
                )
				resourceFile.writeBytes(newDocumentContent.toByteArray())
			}
		}
	}
}